/*Alejandro Parra*/
import Utils from '../Utils/index.js';

export default class{ 

    attr_protected=["type", "foreach", "value"];
    selector_protected=["state.", "props."];
    tag_state_repeat="@@{";
    tag_state_action="@#{";
    tag_prop_repeat="!!{";
    tag_prop_action ="!#";

    //Constructor
    constructor(options, component, )
    {
        this.el= options.el;
        this.data= options.data;
        this.props= options.props;
        this.debug= options.debug;
        this.component= component;
        
        
        
        if(this.debug)
            console.log("<< Component created: ", this.component);
        
    }


    
    //Init
    init(){
        
        if(this.debug)
            console.log("<< Component init");
        this.render();
    }

    //Render UI
    render(){

        if(this.debug)    
        {
            console.log('<<Render inside', this.el);
        }

        const $el= document.querySelector(this.el);
        
        if(!$el)
        {
            if(this.debug)    
            {
                console.error('The element does not exists');
            }

            return;   
        }
        
        if(this.debug)
            console.log('getState', this.getState());


        if(this.debug)
            console.log("<<Preparing Dom",);



        var currenttemplate= this.templateSetVars(Utils.htmlToElements(this.template()));

        $el.innerHTML= currenttemplate.outerHTML;
        


    };

    //Set state
    setState(obj){
        for(let key in  obj)
        {
            if(this.data.hasOwnProperty(key))
            {
                this.data[key]= obj[key];
            }
        }

        this.render();
    };

    //Get state
    getState(){
        return JSON.parse( JSON.stringify(this.data))
    };


    //Set props
    setProps(obj){
        for(let key in  obj)
        {
            if(this.props.hasOwnProperty(key))
            {
                this.props[key]= obj[key];
            }
        }

        this.render();
    };

    //Get props
    getProps(){
        return this.props;
    };


    templateSetVars(dom){
        var self = this; //preserve this obj

        if(!dom.hasChildNodes())
        {

            switch(dom.nodeName)
                {
                    
                    case '#text'://text

                        
                        if(dom.textContent.indexOf(self.tag_state_repeat)!==-1)
                        {//Special content

                            let response= self.templateSetDinnamicState(dom, dom.textContent);
                            dom.textContent="";//delete tmp template
                            return response;
                            //dom.parentNode= self.templateSetDinnamicState(dom, dom.textContent);
                        }
                        else
                        {//normal dom
                            console.log("77777777777777777 normal dom");
                        }
                        
                        break;
                    case 'INPUT'://text

                        
                        if(dom.getAttribute("onChange").indexOf(self.tag_prop_action)===0)
                        {//Special content
                            let action= JSON.parse(dom.getAttribute("onChange").replace(self.tag_prop_action, "").replace(self.tag_prop_action.split("").reverse().join(""), ""));

                            self.action_add(action, "onChange", dom);

                        }
                        else
                        {//normal dom
                            console.log("77777777777777777 normal dom");
                        }
                        
                        break;
                    default:

                        console.error("tenemos un desoocido", dom.nodeName);
                    break;
                }
            
        }
        else
        {
            dom.childNodes.forEach(function(item){
                item= self.templateSetVars(item);
            })

            return dom;
            
        }
        
        
    }


    templateSetDinnamicProps(item){
        const self=this;
        //const realItem= JSON.parse((item.replace(self.tag_prop, "").slice(0,-1)));
        console.log("item", item);
    }


    templateSetDinnamicState(dom, item){

        const self=this;

        let obj= dom.parentNode;
        const realItem= JSON.parse((item.replace(self.tag_state_repeat, "").slice(0,-1)));
        


        
        switch(realItem.type)
        {
            case "li":
                let template=`<li class=":class:">:value:</li>`;
                
                

                let selector_real= self.state_get_type(realItem.foreach);
                

                

                selector_real.forEach(function (value) {

                    let li = document.createElement("li");

                    for(var attr in realItem)
                    {


                        if(!self.attr_protected.includes(attr))
                            li.setAttribute(attr, realItem[attr]);

                    }
                    
                    
                    li.appendChild(document.createTextNode(value));
                    obj.appendChild(li);
                    

                 });

                break;
        }



        

        return "";

    }


    state_get_type(selector)
    {
        

        for(var item in this.selector_protected)
        {
            
            if(selector.indexOf(this.selector_protected[item])===0)
            {
                let selector_real= selector.replace(this.selector_protected[item], "");

                switch(parseInt(item))
                {
                    case 0://state
                        return this.getState()[selector_real];
                    break;
                    case 1://properties
                        return this.props[selector_real];
                        break;
                }
            }
        }

        
    }


    action_add(action, on, dom)
    {
        console.log("action", action[0]);
        console.log("on", on);
        console.log("dom", dom);


        /*FELIPE */

        const vectAction= Object.keys(action)[0].split('.');

        console.log("vectAction", vectAction);

        dom.onchnage = function() { console.log("success") };

        
        /*
        dom.setAttribute(on, (event) => {
            console.log("yeah!"), event;

            switch(this.selector_protected.indexOf(vectAction[0]+"."))
            {
                case 0:
                    console.log("state");
                    break;
                case 1:
                    console.log("props");
                    break
            }
            
        },
        false);
        */

        dom.addEventListener("change",function () {
            alert("Input Changed");
          })



    }
}