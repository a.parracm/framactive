/*Alejandro Parra*/
import Component from './core/Component/index.js';


//export default class App extends Component{
export default class extends Component{ 
    
    //Constructor
    constructor(options) {
        super(options, "Main");//Send the component name
    }
    
    //Template
    template(){
        const liRepeater= JSON.stringify({
            "type": "li",
            "foreach": "props.todoList",
            "value": "value",
            "class": "li_item",
            "onClick" : "self.caller1",
            "onHover" : "self.caller2"
        });
        //return `<ul id="ulSelector">@@{${liRepeater}}</ul>`;

        
        //const component= `<div><h1>hola</h1><div><div><ul id="ulSelector">@@{${liRepeater}}</ul></div></div></div>`;
        const component= `<form><input type="text" id="txtInput" onChange='!#{"props.todoList.add":"old"}#!'><ul id="{{state.user}}">@@{${liRepeater}}</ul></form>`;
        ////const component= `<form><input type="text" id="txtInput" onChangue='aa'><ul id="{{state.user}}">@@{${liRepeater}}</ul></form>`;
        //const component= `<ul id="ulSelector"><li id={{state.user}}>option</li></ul>`;

        return component;
  

        
    }

    //methods
    caller1()
    {
        alert("click");
    }

    caller2()
    {
        alert("click");
    }

    
}

